package com.example.lisp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Game extends AppCompatActivity {

    Options options = Options.getInstance();
    Button buttons[][];
    Board board;
    private int size = options.getSize();
    private int androidsDiscovered = 0;
    private int scansUsed = 0;
    private int ANDROIDS;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        getSupportActionBar().hide();

        buttons = new Button[size][size];

        populateButtons();
        generateBoard();
        mp = MediaPlayer.create(this, R.raw.ping);

        ANDROIDS = board.getANDROIDS();
    }

    private void populateButtons() {
        TableLayout table = (TableLayout) findViewById(R.id.table_buttons);

        for (int row = 0; row < size; row++) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT,
                    1.0f));
            table.addView(tableRow);

            for (int col = 0; col < size; col++) {
                final int finalRow = row;
                final int finalCol = col;
                Button btn = new Button(this);
                btn.setLayoutParams(new TableRow.LayoutParams(
                        TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT,
                        1.0f));

                //Make text visible for big grid
                btn.setPadding(0, 0, 0, 0);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        gridButtonClicked(finalCol, finalRow);
                    }
                });
                tableRow.addView(btn);
                buttons[row][col] = btn;
            }
        }
    }

    private void generateBoard() {
        board = new Board(size);
    }

    private void gridButtonClicked(int col, int row) {
        Button btn = buttons[row][col];

        lockButtonSize();

        int newWidth = btn.getWidth();
        int newHeight = btn.getHeight();
        Bitmap originalBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.android);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(originalBitmap, newWidth, newHeight, true);
        Resources resource = getResources();

        //Check tile
        switch (board.getValue(row, col)) {

            //Use scan
            case 0:
                scansUsed++;
                boolean lootNearby = board.scanPerimeter(row, col);
                if (lootNearby) {
                    mp.start();
                }
                board.setScanned(row, col);
                btn.setEnabled(false);
                btn.setText("Scan");
                break;

            //Scan previously used here (nothing happens)
            case 1:
                break;

            //Mine found here (Sets position to no mine so it can be scanned & # found +1)
            case 2:
                androidsDiscovered++;
                board.setFound(row, col);
                btn.setBackground(new BitmapDrawable(resource, scaledBitmap));
                checkAllLootFound();
                break;
        }
        updateText();
    }

    private void lockButtonSize() {
        for (int row = 0; row < size; row++) {
            for (int col = 0 ; col < size; col++) {
                Button btn = buttons[row][col];

                int width = btn.getWidth();
                btn.setMinWidth(width);
                btn.setMaxWidth(width);

                int height = btn.getHeight();
                btn.setMinHeight(height);
                btn.setMaxHeight(height);
            }
        }
    }

    private void checkAllLootFound() {
        if (androidsDiscovered == ANDROIDS) {
            FragmentManager manager = getSupportFragmentManager();
            WinFragment dialog = new WinFragment();
            dialog.show(manager, "MessageDialog");
        }
    }

    private void updateText() {
        TextView lootLeftText = (TextView) findViewById(R.id.tv_androids_left);
        TextView scansUsedText = (TextView) findViewById(R.id.tv_scans_used);

        lootLeftText.setText("Found " + androidsDiscovered + " of " + ANDROIDS + " loot.");
        scansUsedText.setText("Number of scans used: " + scansUsed);
        lootLeftText.invalidate();
        scansUsedText.invalidate();
    }

    public static Intent makeLaunchIntent(Context context) {
        return new Intent(context, Game.class);
    }
}