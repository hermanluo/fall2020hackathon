package com.example.lisp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Options options = Options.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        Button btn5x5 = (Button) findViewById(R.id.btn_5x5);
        Button btn6x6 = (Button) findViewById(R.id.btn_6x6);
        Button btn7x7 = (Button) findViewById(R.id.btn_7x7);
        btn5x5.setOnClickListener(OnClickListener);
        btn6x6.setOnClickListener(OnClickListener);
        btn7x7.setOnClickListener(OnClickListener);

    }

    View.OnClickListener OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_5x5:
                    options.setSize(5);
                    break;
                case R.id.btn_6x6:
                    options.setSize(6);
                    break;
                case R.id.btn_7x7:
                    options.setSize(7);
                    break;
            }
            startActivity(Game.makeLaunchIntent(MainActivity.this));
        }
    };
}