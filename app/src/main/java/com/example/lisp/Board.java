package com.example.lisp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Board {
    private int[][] board;
    private final int ANDROIDS = 10;
    private int size;

    public Board(int sizeSelected) {
        this.size = sizeSelected;
        board = new int[size][size];
        generateLoot();
    }

    //Generate lists of positions for mines
    private void generateLoot() {

        List<Integer> rowList = new ArrayList<>();
        List<Integer> colList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            rowList.add(i);
            colList.add(i);
        }

        //Generate random positions via shuffling for mines
        for (int k = 0; k < ANDROIDS; k++) {
            Collections.shuffle(rowList);
            Collections.shuffle(colList);
            int rowPosition = rowList.get(0);
            int colPosition = colList.get(0);

            //If no mine at position
            if (board[rowPosition][colPosition] == 0) {
                //Set mine
                board[rowPosition][colPosition] = 2;
            }
            //Else choose another location
            else {
                k--;
            }
        }
    }

    public int getANDROIDS() {
        return ANDROIDS;
    }

    //Get value at position of board (0 = no mine, 1 = scanned, 2 = mine)
    public int getValue(int row, int col) {
        return board[row][col];
    }

    //Set value of position with mine to 0 once found
    public void setFound(int row, int col) {
        board[row][col] = 0;
    }

    //Set value of position without mine to 1 once scanned
    public void setScanned(int row, int col) {
        board[row][col] = 1;
    }

    public boolean scanPerimeter(int row, int col) {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                try {
                    if (board[row + j][col + i] == 2) {
                        return true;
                    }
                }
                catch (ArrayIndexOutOfBoundsException e) { }
            }
        }
        return false;
    }
}
