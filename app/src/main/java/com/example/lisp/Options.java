package com.example.lisp;

public class Options {
    private static Options instance;
    int size;

    private Options() {

    }

    public static Options getInstance() {
        if (instance == null) {
            instance = new Options();
        }
        return instance;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int sizeSelected) {
        this.size = sizeSelected;
    }

}
